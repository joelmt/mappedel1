package edu.ntnu.idatt2001.joelmt;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class responsible for testing methods from the Department class
 *
 * @author joelmt
 */
class DepartmentTest {

    /**
     * Test the remove method using an already existing person
     * to test if it properly removes it
     *
     */
    @Test
    void removeExpectedInput() {
        Department department = new Department("Name");
        Employee employee1 = new Employee("James", "Smith", "12345");
        department.addEmployee(employee1);
        try {
            department.remove(employee1);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        assertFalse(department.getEmployees().contains(employee1));
    }

    /**
     * Test the remove method using a person that does not
     * exist in the department to test if it properly deals
     * deals with it and displays the correct exception message.
     *
     */
    @Test
    void removeUnexpectedInput() {
        Department department = new Department("Name");
        Employee employee1 = new Employee("James", "Smith", "12345");
        Employee employee2 = new Employee("Elisabeth", "Smith", "54321");
        department.addEmployee(employee1);
        try {
            department.remove(employee2);
        } catch (RemoveException e) {
            e.printStackTrace();
            assertEquals(e.toString(), "edu.ntnu.idatt2001.joelmt.RemoveException: The person with the following social security number: 54321, does not exist in the department.");
        }
        assertFalse(department.getEmployees().contains(employee2));
    }
}