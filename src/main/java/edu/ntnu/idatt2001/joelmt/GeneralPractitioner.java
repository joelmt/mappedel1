package edu.ntnu.idatt2001.joelmt;

/**
 * The GeneralPractitioner class is a class representing a specific role under the Doctor superclass.
 * It includes methods that override the abstract methods in the Doctor class and Employee class.
 *
 * @author joelmt
 */
public class GeneralPractitioner extends Doctor{
    GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient p, String diagnosis) {
        p.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return "GeneralPractitioner{" + super.toString() + '}';
    }
}
