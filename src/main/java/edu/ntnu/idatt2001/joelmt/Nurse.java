package edu.ntnu.idatt2001.joelmt;

/**
 * The Nurse class is a class representing a specific role under the Employee superclass.
 * As it is not a subclass of the Doctor class, it does not have access to setting a
 * patients diagnosis.
 *
 * @author joelmt
 */
public class Nurse extends Employee {
    Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse{" + super.toString() + '}';
    }
}
