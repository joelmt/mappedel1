package edu.ntnu.idatt2001.joelmt;

import java.util.Objects;

/**
 * The person class is an abstract superclass for all classes
 * that represents a person.
 *
 * @author joelmt
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;


    Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     * A method for comparing two object of the person type. A person is assumed equal
     * to another if their social security numbers are equal
     *
     * @param o the object you want to compare
     * @return {@code true} if the persons have equal social security numbers
     *         {@code false} if the objects are not equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return '{' + getFullName() + ", " + socialSecurityNumber + '}';
    }
}
