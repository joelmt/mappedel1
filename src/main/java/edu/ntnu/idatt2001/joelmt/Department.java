package edu.ntnu.idatt2001.joelmt;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The Department class represents a department in a hospital.
 * It has list over all employees and patients, and methods for adding and removing
 * people for the lists
 *
 * @author joelmt
 */

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();

    Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Method for adding a new employee to the employees list
     * It only adds the employee if it is not already contained in the list
     *
     * @param newEmployee an employee you want to add to the list
     */
    public void addEmployee(Employee newEmployee) {
        if (!employees.contains(newEmployee)) {
            employees.add(newEmployee);
        }
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Method for adding a new patient to the patients list
     * It only adds the patient if it is not already contained in the list
     *
     * @param newPatient a patient you want to add to the list
     */
    public void addPatient(Patient newPatient) {
        if (!patients.contains(newPatient)) {
            patients.add(newPatient);
        }
    }

    /**
     * Method for removing either a patient or an employee from the
     * employees or patients lists. If the person does not exist in the lists
     * it throws an exception containing the social security number of the person
     * that you wanted to remove.
     *
     * @param p a person you want to remove from a department
     */
    public void remove(Person p) throws RemoveException {
        if (p instanceof Employee && employees.contains(p)) {
            employees.remove(p);
        } else if (p instanceof Patient && patients.contains(p)) {
            patients.remove(p);
        } else {
            throw new RemoveException(p.getSocialSecurityNumber());
        }
    }
    /**
     * A method for comparing two object of the department type. A department is assumed equal
     * to another if the department names are equal
     *
     * @param o the object you want to compare
     * @return {@code true} if the departments have equal names
     *         {@code false} if the objects are not equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        return "Department{" + departmentName +
                ", " + employees +
                ", " + patients +
                '}';
    }
}
