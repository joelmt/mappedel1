package edu.ntnu.idatt2001.joelmt;

/**
 * The patient class represents a person with the subclass patient.
 * The class also implements the interface Diagnosable
 *
 * @author joelmt
 */

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" + super.toString() + ", " + diagnosis + '}';
    }
}
