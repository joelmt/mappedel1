package edu.ntnu.idatt2001.joelmt;

/**
 * The RemoveException class is an exception class which represents a custom
 * exception thrown by the remove method in the Department class if the person does not exists.
 * It creates a custom response message depending on which person is attempted removed from a department
 *
 * @author joelmt
 */

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * RemoveException constructor
     * It is responsible for taking in a string containing a person
     * social security number, and making an exception message
     *
     * @param string a string containing a persons social security number
     */
    public RemoveException(String string) {
        super("The person with the following social security number: " + string + ", does not exist in the department.");
    }
}
