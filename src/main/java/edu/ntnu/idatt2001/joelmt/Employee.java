package edu.ntnu.idatt2001.joelmt;

/**
 * The Employee class represents a person with the subclass employee.
 * It also acts as a superclass for other classes representing
 * more specific employee-roles
 *
 * @author joelmt
 */
public class Employee extends Person {
    Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Employee{" + super.toString() + '}';
    }
}
