package edu.ntnu.idatt2001.joelmt;

import java.util.ArrayList;

/**
 * A simple client program to test the functionality of certain methods.
 * It utilizes the fillRegisterWithTestData method from HospitalTestData, and
 * it uses the remove method from the Department class both with an existing and a
 * non-existing person.
 *
 * @author joelmt
 */
public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = new Hospital("Norton Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);
        ArrayList<Department> departmentArrayList = hospital.getDepartments();
        try {
            departmentArrayList.get(0).remove(new Employee("Odd Even", "Primtallet", "458597125"));
        } catch (RemoveException e) {
            System.out.println(e);
        }

        try {
            departmentArrayList.get(0).remove(new Patient("Siri", "Langbakken", "926601829"));
        } catch (RemoveException e) {
            System.out.println(e.toString());
        }
        System.out.println(hospital.toString());

    }
}
