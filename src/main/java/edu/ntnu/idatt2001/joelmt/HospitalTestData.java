package edu.ntnu.idatt2001.joelmt;

/**
 * A simple class responsible for filling an instance of the Hospital object with
 * test data.
 *
 */

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }

    /**
     * @param hospital hospital you want to fill with data
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "458597125"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "727381955"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "191081286"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "128534277"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "349313532"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "486340287"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "449490349"));
        emergency.addPatient(new Patient("Inga", "Lykke", "907632816"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "593740707"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "245411069"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "885904705"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "579223905"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "976697629"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "100183186"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "605277714"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "630627767"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "869194137"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "909783076"));
        hospital.addDepartment(childrenPolyclinic);
    }
}
