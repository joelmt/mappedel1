package edu.ntnu.idatt2001.joelmt;

import java.util.ArrayList;

/**
 * The Hospital class represents a hospital consisting of multiple departments.
 * It has list over all the hospitals departments.
 *
 * @author joelmt
 */

public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departmentList = new ArrayList<>();

    Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departmentList;
    }

    /**
     * Method for adding a new department to the department list
     * It only adds the department if it is not already contained in the list
     *
     * @param newDepartment a department you want to add to the list
     */
    public void addDepartment(Department newDepartment) {
        if (!departmentList.contains(newDepartment)) {
            departmentList.add(newDepartment);
        }
    }

    @Override
    public String toString() {
        return "Hospital{" + hospitalName + '\'' +
                ", departmentList=" + departmentList +
                '}';
    }
}
