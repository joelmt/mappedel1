package edu.ntnu.idatt2001.joelmt;

/**
 * The Diagnosable interface contains a method
 * for setting a diagnosis for classes that implements it.
 *
 * @author joelmt
 */
public interface Diagnosable {
    public void setDiagnosis(String diagnosis);
}
