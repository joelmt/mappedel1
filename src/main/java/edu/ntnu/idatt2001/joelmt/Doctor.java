package edu.ntnu.idatt2001.joelmt;

/**
 * The Doctor class is an abstract superclass for all classes
 * that represents a specific role within the doctor-term.
 * It also represents the only classes able to set a patients diagnosis
 *
 * @author joelmt
 */
public abstract class Doctor extends Employee {
    Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient p, String diagnosis);
}
